# Create Password

Creating a strong password with personalization capabilities

## Getting started & Usage

To create the desired password, it is necessary to make some changes in the source code:
‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍‍
```python
create_password(length=8, upper=True, lower=True, digit=True, pun=True)
```
### length=8 :: the length of password, by Defult is 8.
### upper=True :: use uppercase for password, by Defult is True
### lower=True :: use lowercase for password, by Defult is True
### digit=True :: use numbers for password, by Defult is True
### pun=True :: use punctuation for password, by Defult is True

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
